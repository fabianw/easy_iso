include ./Makefile.config

CC = mpic++
# CC = g++

HDR = $(wildcard src/*.h)
.PHONY: clean third

easyIso: src/easy_iso.o
	$(CC) $(CPPFLAGS) $(INC) -o easyIso++ src/easy_iso.o $(LIB)

easyIsoMPI: src/easy_iso_MPI.o
	$(CC) $(CPPFLAGS) $(INC) -o easyIsoMPI++ src/easy_iso_MPI.o $(LIB)

third:
	cd third_party; \
	$(MAKE) CC=$(CC) -f Makefile cleanall; \
	$(MAKE) CC=$(CC) -f Makefile all

%.o: %.cpp
	$(CC) $(CPPFLAGS) $(INC) -c $< -o $@

clean:
	find . -iname "*~" -exec rm -f {} \;
	rm -f src/*.o
	rm -f easyIso++ easyIsoMPI++
