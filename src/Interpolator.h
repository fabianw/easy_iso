// File       : Interpolator.h
// Date       : Tue Nov 22 15:37:00 2016
// Author     : Fabian Wermelinger
// Description: Data interpolator type
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef INTERPOLATOR_H_QCK6H0CI
#define INTERPOLATOR_H_QCK6H0CI

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstring>
#include <string>
#include <functional>
#include <chrono>
#ifdef _USE_HDF_
#include <hdf5.h>
#endif /* _USE_HDF_ */

#include "common.h"
#include "ArgumentParser.h"
#ifdef _USE_CUBISMZ_
#include "Reader_WaveletCompression.h"
#endif /* _USE_CUBISMZ_ */


template <typename TKernel>
class Interpolator
{
public:
    Interpolator() = default;
    Interpolator(ArgumentParser& p) :
        m_extent(p("extent").asDouble(1.0))
    {
        // load the data
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        if (p("load").asString("h5") == "h5")           _load_h5(p);
        else if (p("load").asString("h5") == "wavelet") _load_wavelet(p);
        else
        {
            std::cerr << "ERROR: Interpolator: Undefined loader \"" << p("load").asString() << "\"" << std::endl;
            abort();
        }
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        std::cout << "Loading data done. Elapsed time = " << elapsed_seconds.count() << "s" << std::endl;

        // cell centered or nodal values?
        if (p("data").asString("cell") == "cell")      _nCells = [](const int N) { return N; };
        else if (p("data").asString("cell") == "node") _nCells = [](const int N) { return N-1; };
        else
        {
            std::cerr << "ERROR: Interpolator: Unknown data attribute \"" << p("data").asString() << "\"" << std::endl;
            abort();
        }

        const int Nmax = std::max(m_data.Nx(), std::max(m_data.Ny(), m_data.Nz()));
        assert(Nmax > 1);
        m_h = m_extent/_nCells(Nmax);
        m_invh = 1.0/m_h;
        m_extentX = m_h*_nCells(m_data.Nx());
        m_extentY = m_h*_nCells(m_data.Ny());
        m_extentZ = m_h*_nCells(m_data.Nz());

        // init coordinate transform
        _physicalDataPos = [this](const int i) { return i*this->m_h; };
        m_origin_off[0] = 0.0;
        m_origin_off[1] = 0.0;
        m_origin_off[2] = 0.0;
        m_isNodal = 1;
        if (p("data").asString("cell") == "cell")
        {
            _physicalDataPos = [this](const int i) { return (0.5+i)*this->m_h; };
            m_origin_off[0] = 0.5*m_h;
            m_origin_off[1] = 0.5*m_h;
            m_origin_off[2] = 0.5*m_h;
            m_isNodal = 0;
        }
    }
    virtual ~Interpolator() {}

    Real operator()(const Real x, const Real y, const Real z) const
    {
        const Real xo = x - m_origin_off[0];
        const Real yo = y - m_origin_off[1];
        const Real zo = z - m_origin_off[2];

        const int ix0 = _idx(xo);
        const int iy0 = _idx(yo);
        const int iz0 = _idx(zo);

        Real interp = 0.0;
        for (int iz = TKernel::start; iz < TKernel::end; ++iz)
            for (int iy = TKernel::start; iy < TKernel::end; ++iy)
                for (int ix = TKernel::start; ix < TKernel::end; ++ix)
                {
                    const Real xj = _pos(ix0+ix);
                    const Real yj = _pos(iy0+iy);
                    const Real zj = _pos(iz0+iz);
                    const Real Mx = m_kernel((xo-xj)*m_invh);
                    const Real My = m_kernel((yo-yj)*m_invh);
                    const Real Mz = m_kernel((zo-zj)*m_invh);
                    interp += m_data(ix0+ix,iy0+iy,iz0+iz)*Mx*My*Mz;
                }
        return interp;
    }

    inline Real getH() const { return m_h; }
    inline Real getExtent() const { return m_extent; }
    inline Real getExtentX() const { return m_extentX; }
    inline Real getExtentY() const { return m_extentY; }
    inline Real getExtentZ() const { return m_extentZ; }
    inline Real getDataPos(const int i) const { return _physicalDataPos(i); }
    inline void setOrigin(const Real o[3]) { for (int i=0; i<3; ++i) m_origin_off[i] = o[i]; }
    inline int  getNx() const { return m_data.Nx(); }
    inline int  getNy() const { return m_data.Ny(); }
    inline int  getNz() const { return m_data.Nz(); }
    inline int  isNodal() const { return m_isNodal; }

protected:
    Real m_extent;
    Real m_extentX;
    Real m_extentY;
    Real m_extentZ;
    Real m_origin_off[3];
    Real m_h, m_invh;
    int m_isNodal;

    Matrix_t m_data;
    TKernel m_kernel;

    // helper
    std::function<int(const int)> _nCells;
    std::function<Real(const int)> _physicalDataPos;
    inline int _idx(const Real x) const { return std::floor(x*m_invh); }
    inline Real _pos(const int i) const { return i*m_h; }

private:
    // loader
    void _load_h5(ArgumentParser& p);
    void _load_wavelet(ArgumentParser& p);
};


template <typename TKernel>
void Interpolator<TKernel>::_load_h5(ArgumentParser& parser)
{
#ifdef _USE_HDF_
    const std::string group = parser("hdf5_group").asString("/data");
    const std::string filename = parser("file").asString("");
    if (filename == "")
    {
        std::cerr << "ERROR: Interpolator: -file is not specified. No input file given" << std::endl;
        abort();
    }

    hid_t file_id, dataset_id, dataspace_id, file_dataspace_id;
    hsize_t* dims;
    hssize_t num_elem;
    int rank, ndims, NCH;
    int maxDim[3];
    Real* data;

    file_id           = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    dataset_id        = H5Dopen(file_id, group.c_str(), H5P_DEFAULT);
    file_dataspace_id = H5Dget_space(dataset_id);
    rank              = H5Sget_simple_extent_ndims(file_dataspace_id);
    dims              = new hsize_t[rank];
    ndims             = H5Sget_simple_extent_dims(file_dataspace_id, dims, NULL);
    num_elem          = H5Sget_simple_extent_npoints(file_dataspace_id);
    data              = new Real[num_elem];
    maxDim[2]         = dims[0];
    maxDim[1]         = dims[1];
    maxDim[0]         = dims[2];
    NCH               = dims[3];
    dataspace_id      = H5Screate_simple(rank, dims, NULL);
    int status        = H5Dread(dataset_id, HDF_PRECISION, dataspace_id, file_dataspace_id, H5P_DEFAULT, data);

    /* release stuff */
    delete [] dims;
    status = H5Dclose(dataset_id);
    status = H5Sclose(dataspace_id);
    status = H5Sclose(file_dataspace_id);
    status = H5Fclose(file_id);

    if (NCH > 1)
    {
        const int channel    = parser("channel").asInt(0); // data channel
        const bool magnitude = parser("magnitude").asBool(false); // vector magnitude (only if NCH == 3)
        if (magnitude && NCH == 3)
        {
            int k = 0;
            for (int i = 0; i < num_elem; i += NCH)
            {
                const Real a = data[i+0];
                const Real b = data[i+1];
                const Real c = data[i+2];
                data[k++] = std::sqrt(a*a + b*b + c*c);
            }

        }
        else
        {
            assert(channel < NCH);
            int k = 0;
            for (int i = 0; i < num_elem; i += NCH)
                data[k++] = data[i+channel];
        }
    }
    this->m_data = std::move(Matrix_t(maxDim[0], maxDim[1], maxDim[2], data));
    delete [] data;
#endif /* _USE_HDF_ */
}

template <typename TKernel>
void Interpolator<TKernel>::_load_wavelet(ArgumentParser& p)
{
#ifdef _USE_CUBISMZ_
    const bool byte_swap   = p("-swap").asBool(false);
    const int wavelet_type = p("-wtype").asInt(1);
    const std::string filename = p("file").asString("");
    if (filename == "")
    {
        std::cerr << "ERROR: Interpolator: -file is not specified. No input file given" << std::endl;
        abort();
    }

    Reader_WaveletCompression myreader(filename, byte_swap, wavelet_type);
    myreader.load_file();
    const int NBX = myreader.xblocks();
    const int NBY = myreader.yblocks();
    const int NBZ = myreader.zblocks();
    const int maxDim[3] = {NBX*_BLOCKSIZE_, NBY*_BLOCKSIZE_, NBZ*_BLOCKSIZE_};
    const size_t num_elem = static_cast<size_t>(maxDim[0]) * maxDim[1] * maxDim[2];
    Real* const data = new Real[num_elem];

    const int nBlocks = NBX * NBY * NBZ;
#pragma omp parallel
    {
        Real blockdata[_BLOCKSIZE_][_BLOCKSIZE_][_BLOCKSIZE_];
#pragma omp for
        for (int i = 0; i < nBlocks; ++i)
        {
            const int ix = i%NBX;
            const int iy = (i/NBX)%NBY;
            const int iz = (i/(NBX*NBY))%NBZ;
            const double zratio = myreader.load_block2(ix, iy, iz, blockdata);

            for (int z=0; z < _BLOCKSIZE_; ++z)
                for (int y=0; y < _BLOCKSIZE_; ++y)
                {
                    assert(iy*_BLOCKSIZE_+y < maxDim[1]);
                    assert(iz*_BLOCKSIZE_+z < maxDim[2]);
                    const size_t offset = _BLOCKSIZE_*(static_cast<size_t>(ix) + NBX*(y+iy*_BLOCKSIZE_ + (z+static_cast<size_t>(iz)*_BLOCKSIZE_)*NBY*_BLOCKSIZE_));
                    Real* const dst = data + offset;
                    memcpy(dst, &blockdata[z][y][0], _BLOCKSIZE_*sizeof(Real));
                }
        }
    }

    this->m_data = std::move(Matrix_t(maxDim[0], maxDim[1], maxDim[2], data));
    delete [] data;
#else
    fprintf(stderr, "WARNING: Executable was compiled without wavelet compressor support...\n");
#endif /* _USE_CUBISMZ_ */
}

#endif /* INTERPOLATOR_H_QCK6H0CI */
