// File       : easy_iso.cpp
// Date       : Tue Nov 22 15:00:14 2016
// Author     : Fabian Wermelinger
// Description: Isosurface extractor
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <string>
#include <sstream>
#include "ArgumentParser.h"
#include "interpolation/kernels.h"
#include "IsoExtractor.h"

int main(int argc, char* argv[])
{
    ArgumentParser parser(argc, const_cast<const char**>(argv));

    using Tinterp = M2;
    if (parser("interp_kernel").asString("M2") == "Mp4")
        using Tinterp = Mprime4;
    else if (parser("interp_kernel").asString("M2") == "M5")
        using Tinterp = M5;

    IsoExtractor<Tinterp> iso(parser);

    const Real isoval = parser("isoval").asDouble(0.0);
    std::ostringstream basename;
    basename << "isosurf_" << isoval;
    const std::string fout = parser("fout").asString(basename.str());
    iso.extract(isoval, fout);

    return 0;
}
