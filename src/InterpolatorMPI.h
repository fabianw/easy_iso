// File       : InterpolatorMPI.h
// Date       : Mon Nov 28 16:15:41 2016
// Author     : Fabian Wermelinger
// Description: MPI'ed Interpolator
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef INTERPOLATORMPI_H_FKS9KH2J
#define INTERPOLATORMPI_H_FKS9KH2J

#include <cassert>
#include <vector>
#include <cmath>
#include <functional>
#include <mpi.h>
#include "Interpolator.h"

#ifdef _FLOAT_PRECISION_
#define MPIREAL MPI_FLOAT
#else
#define MPIREAL MPI_DOUBLE
#endif

class MPInbr
{
public:
    MPInbr(const MPI_Comm comm)
    {
        // assumes comm is a communicator created with MPI_Cart_create
        int myidx[3];
        int dims[3];
        int periods[3];
        MPI_Cart_get(comm, 3, dims, periods, myidx);

        for (int i=0; i<27; ++i)
        {
            m_nbr[i] = -1;
            int off[3] = {(i%3)-1, ((i/3)%3)-1, (i/9)-1};
            int coords[3];
            for (int j=0; j<3; ++j)
                coords[j] = myidx[j] + off[j];

            if (coords[0] < 0 || coords[0] >= dims[0]) continue;
            if (coords[1] < 0 || coords[1] >= dims[1]) continue;
            if (coords[2] < 0 || coords[2] >= dims[2]) continue;

            int& nbr = m_nbr[(off[0]+1) + (off[1]+1)*3 + (off[2]+1)*9];
            MPI_Cart_rank(comm, coords, &nbr);
        }
    }

    inline int operator()(const int ix, const int iy, const int iz) const
    {
        assert(ix >= -1 && ix <= 1);
        assert(iy >= -1 && iy <= 1);
        assert(iz >= -1 && iz <= 1);
        return m_nbr[(ix+1) + (iy+1)*3 + (iz+1)*9];
    }

private:
    int m_nbr[27];
};

struct CubeHalo
{
    Faces faces;
    Edges edges;
    Corners corners;

    CubeHalo() {}
    CubeHalo(const int Nx, const int Ny, const int Nz) :
        faces(Ny,Nz, Nx,Nz, Nx,Ny),
        edges(Nx, Ny, Nz)
    {}
    void alloc(const int Nx, const int Ny, const int Nz)
    {
        faces.alloc(Ny,Nz, Nx,Nz, Nx,Ny);
        edges.alloc(Nx, Ny, Nz);
    }
};

template <typename Tinterp>
class InterpolatorMPI : public Interpolator<Tinterp>
{
public:
    InterpolatorMPI(ArgumentParser& p, const MPI_Comm comm=MPI_COMM_WORLD) :
        Interpolator<Tinterp>(),
        m_PESize{p("xpesize").asInt(1),p("ypesize").asInt(1),p("zpesize").asInt(1)}
    {
        this->m_extent = p("extent").asDouble(1.0);
        int periodic[3] = {false};

        MPI_Cart_create(comm, 3, m_PESize, periodic, true, &m_cartcomm);

        int world_size;
        MPI_Comm_size(m_cartcomm, &world_size);
        assert(m_PESize[0]*m_PESize[1]*m_PESize[2] == world_size);

        int myrank;
        MPI_Comm_rank(m_cartcomm, &myrank);
        MPI_Cart_coords(m_cartcomm, myrank, 3, m_myPEIndex);
        m_isroot = (0 == myrank);

        // load the data
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        if (p("load").asString("h5") == "h5")           this->_load_h5_MPI(p);
        else if (p("load").asString("h5") == "wavelet") this->_load_wavelet_MPI(p);
        else
        {
            if (m_isroot)
                std::cerr << "ERROR: InterpolatorMPI: Undefined loader \"" << p("load").asString() << "\"" << std::endl;
            abort();
        }
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed_seconds = end-start;
        if (m_isroot)
            std::cout << "Loading data done. Elapsed time = " << elapsed_seconds.count() << "s" << std::endl;

        // cell centered or nodal values?
        if (p("data").asString("cell") == "cell")      this->_nCells = [](const int N) { return N; };
        else if (p("data").asString("cell") == "node") this->_nCells = [](const int N) { return N-1; };
        else
        {
            if (m_isroot)
                std::cerr << "ERROR: InterpolatorMPI: Unknown data attribute \"" << p("data").asString() << "\"" << std::endl;
            abort();
        }

        const int Nx = m_PESize[0]*this->m_data.Nx();
        const int Ny = m_PESize[1]*this->m_data.Ny();
        const int Nz = m_PESize[2]*this->m_data.Nz();
        const int Nmax = std::max(Nx, std::max(Ny, Nz));
        assert(Nmax > 1);
        this->m_h = this->m_extent/this->_nCells(Nmax);
        this->m_invh = 1.0/this->m_h;
        this->m_extentX = this->m_h*this->_nCells(Nx);
        this->m_extentY = this->m_h*this->_nCells(Ny);
        this->m_extentZ = this->m_h*this->_nCells(Nz);

        // init coordinate transform
        this->_physicalDataPos = [this](const int i) { return i*this->m_h; };
        this->m_isNodal = 1;
        if (p("data").asString("cell") == "cell")
        {
            this->_physicalDataPos = [this](const int i) { return (0.5+i)*this->m_h; };
            this->m_isNodal = 0;
        }
        const Real Ox = (this->m_extentX / m_PESize[0]) * m_myPEIndex[0];
        const Real Oy = (this->m_extentY / m_PESize[1]) * m_myPEIndex[1];
        const Real Oz = (this->m_extentZ / m_PESize[2]) * m_myPEIndex[2];
        Real origin[3];
        origin[0] = this->_physicalDataPos(ceil(Ox*this->m_invh)) - Ox;
        origin[1] = this->_physicalDataPos(ceil(Oy*this->m_invh)) - Oy;
        origin[2] = this->_physicalDataPos(ceil(Oz*this->m_invh)) - Oz;
        assert(origin[0] >= 0.0);
        assert(origin[1] >= 0.0);
        assert(origin[2] >= 0.0);
        for (int i = 0; i < 3; ++i)
            this->m_origin_off[i] = origin[i];

        // fetch halo cells
        m_sendbuf.alloc(this->m_data.Nx(), this->m_data.Ny(), this->m_data.Nz());
        m_recvbuf.alloc(this->m_data.Nx(), this->m_data.Ny(), this->m_data.Nz());

        start = std::chrono::system_clock::now();
        _fetch_halo();
        end = std::chrono::system_clock::now();
        elapsed_seconds = end-start;
        if (m_isroot)
            std::cout << "Exchanging messages data done. Elapsed time = " << elapsed_seconds.count() << "s" << std::endl;
    }
    virtual ~InterpolatorMPI() {}

    inline int getNx() const { return m_PESize[0]*this->m_data.Nx(); }
    inline int getNy() const { return m_PESize[1]*this->m_data.Ny(); }
    inline int getNz() const { return m_PESize[2]*this->m_data.Nz(); }
    inline int getLocalNx() const { return this->m_data.Nx(); }
    inline int getLocalNy() const { return this->m_data.Ny(); }
    inline int getLocalNz() const { return this->m_data.Nz(); }
    inline MPI_Comm getComm() const { return m_cartcomm; }
    inline void getPESize(int ps[3]) const { for (int i = 0; i < 3; ++i) ps[i] = m_PESize[i]; }
    inline void getPEIndex(int pi[3]) const { for (int i = 0; i < 3; ++i) pi[i] = m_myPEIndex[i]; }
    inline bool isroot() const {return m_isroot; }


private:
    MPI_Comm m_cartcomm;
    int m_PESize[3];
    int m_myPEIndex[3];
    bool m_isroot;

    // loader
    void _load_h5_MPI(ArgumentParser& p);
    void _load_wavelet_MPI(ArgumentParser& p);

    // halo handling
    using func_t = std::function<void(const int, const int, const int)>;
    CubeHalo m_sendbuf;
    CubeHalo m_recvbuf;
    void _fetch_halo();

    void _send_faces(const MPInbr& nbrs, const Faces& f, std::vector<MPI_Request>& f_req) const;
    void _recv_faces(const MPInbr& nbrs, Faces& f, std::vector<MPI_Request>& f_req) const;
    void _pack_faces(Faces& f, const MPInbr& nbr);
    void _unpack_faces(const Faces& f, const MPInbr& nbr);
    inline void _process_faceX(func_t& eval);
    inline void _process_faceY(func_t& eval);
    inline void _process_faceZ(func_t& eval);
    inline void _pack_faceX(Face_t& f, const int _s0);
    inline void _pack_faceY(Face_t& f, const int _s0);
    inline void _pack_faceZ(Face_t& f, const int _s0);
    inline void _upack_faceX(const Face_t& f, const int _d0);
    inline void _upack_faceY(const Face_t& f, const int _d0);
    inline void _upack_faceZ(const Face_t& f, const int _d0);
    inline void _halo_faceX(const int _s0, const int _d0);
    inline void _halo_faceY(const int _s0, const int _d0);
    inline void _halo_faceZ(const int _s0, const int _d0);

    void _send_edges(const MPInbr& nbrs, const Edges& e, std::vector<MPI_Request>& e_req) const;
    void _recv_edges(const MPInbr& nbrs, Edges& e, std::vector<MPI_Request>& e_req) const;
    void _pack_edges(Edges& f, const MPInbr& nbr);
    void _unpack_edges(const Edges& e, const MPInbr& nbr);
    inline void _process_edgeX(func_t& eval);
    inline void _process_edgeY(func_t& eval);
    inline void _process_edgeZ(func_t& eval);
    inline void _pack_edgeX(Edge_t& e, const int _s0, const int _s1);
    inline void _pack_edgeY(Edge_t& e, const int _s0, const int _s1);
    inline void _pack_edgeZ(Edge_t& e, const int _s0, const int _s1);
    inline void _upack_edgeX(const Edge_t& e, const int _d0, const int _d1);
    inline void _upack_edgeY(const Edge_t& e, const int _d0, const int _d1);
    inline void _upack_edgeZ(const Edge_t& e, const int _d0, const int _d1);
    inline void _halo_edgeX(const int _s0, const int _s1, const int _d0, const int _d1);
    inline void _halo_edgeY(const int _s0, const int _s1, const int _d0, const int _d1);
    inline void _halo_edgeZ(const int _s0, const int _s1, const int _d0, const int _d1);

    void _send_corners(const MPInbr& nbrs, const Corners& c, std::vector<MPI_Request>& c_req) const;
    void _recv_corners(const MPInbr& nbrs, Corners& c, std::vector<MPI_Request>& c_req) const;
    void _pack_corners(Corners& c, const MPInbr& nbr);
    void _unpack_corners(const Corners& c, const MPInbr& nbr);
    inline void _process_corner(func_t& eval);
    inline void _pack_corner(Corner_t& c, const int _s0, const int _s1, const int _s2);
    inline void _upack_corner(const Corner_t& c, const int _d0, const int _d1, const int _d2);
    inline void _halo_corner(const int _s0, const int _s1, const int _s2, const int _d0, const int _d1, const int _d2);
};


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_load_h5_MPI(ArgumentParser& parser)
{
#ifdef _USE_HDF_
    const std::string group = parser("hdf5_group").asString("/data");
    const std::string filename = parser("file").asString("");
    if (filename == "")
    {
        if (m_isroot)
            std::cerr << "ERROR: InterpolatorMPI: -file is not specified. No input file given" << std::endl;
        abort();
    }

    herr_t status;
    hid_t file_id, dataset_id, dataspace_id, file_dataspace_id, fspace_id, fapl_id, mspace_id;
    int ndims, NCH;
    int maxDim[4];

    if (m_isroot)
    {
        hsize_t dims[4];
        file_id           = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
        dataset_id        = H5Dopen(file_id, group.c_str(), H5P_DEFAULT);
        file_dataspace_id = H5Dget_space(dataset_id);
        ndims             = H5Sget_simple_extent_dims(file_dataspace_id, dims, NULL);
        status = H5Dclose(dataset_id);
        status = H5Sclose(file_dataspace_id);
        status = H5Fclose(file_id);
        maxDim[2] = dims[0];
        maxDim[1] = dims[1];
        maxDim[0] = dims[2];
        maxDim[3] = dims[3];
    }
    MPI_Bcast(maxDim, 4, MPI_INT, 0, m_cartcomm);
    NCH = maxDim[3];

    if (maxDim[0] % m_PESize[0] != 0)
    {
        if (m_isroot)
            std::cerr << "ERROR: InterpolatorMPI: Number of processes in x-direction is not an integer multiple of available data." << std::endl;
        abort();
    }
    if (maxDim[1] % m_PESize[1] != 0)
    {
        if (m_isroot)
            std::cerr << "ERROR: InterpolatorMPI: Number of processes in y-direction is not an integer multiple of available data." << std::endl;
        abort();
    }
    if (maxDim[2] % m_PESize[2] != 0)
    {
        if (m_isroot)
            std::cerr << "ERROR: InterpolatorMPI: Number of processes in z-direction is not an integer multiple of available data." << std::endl;
        abort();
    }
    const size_t NX = maxDim[0]/m_PESize[0];
    const size_t NY = maxDim[1]/m_PESize[1];
    const size_t NZ = maxDim[2]/m_PESize[2];

    const size_t num_elem = NX*NY*NZ*NCH;
    Real* const data = new Real[num_elem];

    hsize_t count[4] = {NZ, NY, NX, NCH};
    hsize_t dims[4] = {maxDim[2], maxDim[1], maxDim[0], NCH};
    hsize_t offset[4] = {m_myPEIndex[2]*NZ, m_myPEIndex[1]*NY, m_myPEIndex[0]*NX, 0};

    H5open();
    fapl_id = H5Pcreate(H5P_FILE_ACCESS);
    status = H5Pset_fapl_mpio(fapl_id, m_cartcomm, MPI_INFO_NULL);
    file_id = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, fapl_id);
    status = H5Pclose(fapl_id);

    dataset_id = H5Dopen2(file_id, group.c_str(), H5P_DEFAULT);
    fapl_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(fapl_id, H5FD_MPIO_COLLECTIVE);

    fspace_id = H5Dget_space(dataset_id);
    H5Sselect_hyperslab(fspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

    mspace_id = H5Screate_simple(4, count, NULL);
    status = H5Dread(dataset_id, HDF_PRECISION, mspace_id, fspace_id, fapl_id, data);

    if (NCH > 1)
    {
        const int channel    = parser("channel").asInt(0); // data channel
        const bool magnitude = parser("magnitude").asBool(false); // vector magnitude (only if NCH == 3)
        if (magnitude && NCH == 3)
        {
            int k = 0;
            for (int i = 0; i < num_elem; i += NCH)
            {
                const Real a = data[i+0];
                const Real b = data[i+1];
                const Real c = data[i+2];
                data[k++] = std::sqrt(a*a + b*b + c*c);
            }

        }
        else
        {
            assert(channel < NCH);
            int k = 0;
            for (int i = 0; i < num_elem; i += NCH)
                data[k++] = data[i+channel];
        }
    }
    this->m_data = std::move(Matrix_t(NX, NY, NZ, data));
    delete [] data;
#endif /* _USE_HDF_ */
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_load_wavelet_MPI(ArgumentParser& p)
{
#ifdef _USE_CUBISMZ_
    const bool byte_swap   = p("-swap").asBool(false);
    const int wavelet_type = p("-wtype").asInt(1);
    const std::string filename = p("file").asString("");
    if (filename == "")
    {
        if (m_isroot)
            std::cerr << "ERROR: InterpolatorMPI: -file is not specified. No input file given" << std::endl;
        abort();
    }

    Reader_WaveletCompressionMPI myreader(m_cartcomm, filename, byte_swap, wavelet_type);
    myreader.load_file();
    const int NBX = myreader.xblocks();
    const int NBY = myreader.yblocks();
    const int NBZ = myreader.zblocks();
    assert(NBX % m_PESize[0] == 0);
    assert(NBY % m_PESize[1] == 0);
    assert(NBZ % m_PESize[2] == 0);
    const int myNBX = NBX/m_PESize[0];
    const int myNBY = NBY/m_PESize[1];
    const int myNBZ = NBZ/m_PESize[2];

    const int maxDim[3] = {myNBX*_BLOCKSIZE_, myNBY*_BLOCKSIZE_, myNBZ*_BLOCKSIZE_};
    const size_t num_elem = static_cast<size_t>(maxDim[0]) * maxDim[1] * maxDim[2];
    Real* const data = new Real[num_elem];

    const int nBlocks = myNBX * myNBY * myNBZ;
#pragma omp parallel
    {
        Real blockdata[_BLOCKSIZE_][_BLOCKSIZE_][_BLOCKSIZE_];
#pragma omp for
        for (int i = 0; i < nBlocks; ++i)
        {
            const int ix = i%myNBX;
            const int iy = (i/myNBX)%myNBY;
            const int iz = (i/(myNBX*myNBY))%myNBZ;
            const double zratio = myreader.load_block2(
                    ix+myNBX*m_myPEIndex[0],
                    iy+myNBY*m_myPEIndex[1],
                    iz+myNBZ*m_myPEIndex[2],
                    blockdata);

            for (int z=0; z < _BLOCKSIZE_; ++z)
                for (int y=0; y < _BLOCKSIZE_; ++y)
                {
                    assert(iy*_BLOCKSIZE_+y < maxDim[1]);
                    assert(iz*_BLOCKSIZE_+z < maxDim[2]);
                    const size_t offset = _BLOCKSIZE_*(static_cast<size_t>(ix) + myNBX*(y+iy*_BLOCKSIZE_ + (z+static_cast<size_t>(iz)*_BLOCKSIZE_)*myNBY*_BLOCKSIZE_));
                    Real* const dst = data + offset;
                    memcpy(dst, &blockdata[z][y][0], _BLOCKSIZE_*sizeof(Real));
                }
        }
    }

    this->m_data = std::move(Matrix_t(maxDim[0], maxDim[1], maxDim[2], data));
    delete [] data;
#else
    fprintf(stderr, "WARNING: Executable was compiled without wavelet compressor support...\n");
#endif /* _USE_CUBISMZ_ */
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_fetch_halo()
{
    // concept:
    // 0.) prepare neighbor info
    // 1.) pack buffers and send asynchronous messages
    // 2.) unpack buffers

    // 0.)
    MPInbr mynbrs(m_cartcomm);

    // 1.)
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    // faces
    std::vector<MPI_Request> fpend_recv;
    _recv_faces(mynbrs, m_recvbuf.faces, fpend_recv);

    std::vector<MPI_Request> fpend_send;
    _pack_faces(m_sendbuf.faces, mynbrs);
    _send_faces(mynbrs, m_sendbuf.faces, fpend_send);

    // edges
    std::vector<MPI_Request> epend_recv;
    _recv_edges(mynbrs, m_recvbuf.edges, epend_recv);

    std::vector<MPI_Request> epend_send;
    _pack_edges(m_sendbuf.edges, mynbrs);
    _send_edges(mynbrs, m_sendbuf.edges, epend_send);

    // corners
    std::vector<MPI_Request> cpend_recv;
    _recv_corners(mynbrs, m_recvbuf.corners, cpend_recv);

    std::vector<MPI_Request> cpend_send;
    _pack_corners(m_sendbuf.corners, mynbrs);
    _send_corners(mynbrs, m_sendbuf.corners, cpend_send);

    // 2.)
    std::vector<MPI_Status> fstat_recv(fpend_recv.size());
    MPI_Waitall(fpend_recv.size(), fpend_recv.data(), fstat_recv.data());
    _unpack_faces(m_recvbuf.faces, mynbrs);

    std::vector<MPI_Status> estat_recv(epend_recv.size());
    MPI_Waitall(epend_recv.size(), epend_recv.data(), estat_recv.data());
    _unpack_edges(m_recvbuf.edges, mynbrs);

    std::vector<MPI_Status> cstat_recv(cpend_recv.size());
    MPI_Waitall(cpend_recv.size(), cpend_recv.data(), cstat_recv.data());
    _unpack_corners(m_recvbuf.corners, mynbrs);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_send_faces(const MPInbr& nbrs, const Faces& f, std::vector<MPI_Request>& f_req) const
{
    const int dst[6] = {nbrs(-1,0,0), nbrs(1,0,0), nbrs(0,-1,0), nbrs(0,1,0), nbrs(0,0,-1), nbrs(0,0,1)};
    Real* const data[6] = {f.x0.data, f.x1.data, f.y0.data, f.y1.data, f.z0.data, f.z1.data};
    const int Nelem[6] = {f.x0._Nelem, f.x1._Nelem, f.y0._Nelem, f.y1._Nelem, f.z0._Nelem, f.z1._Nelem};
    const int tagbase = 0;
    for (int i = 0; i < 6; ++i)
    {
        const int dir  = i/2;
        const int side = i%2;
        if (dst[i] >= 0)
        {
            MPI_Request req;
            MPI_Isend(data[i], Nelem[i], MPIREAL, dst[i], tagbase+dir*2+side, m_cartcomm, &req);
            f_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_recv_faces(const MPInbr& nbrs, Faces& f, std::vector<MPI_Request>& f_req) const
{
    const int src[6] = {nbrs(-1,0,0), nbrs(1,0,0), nbrs(0,-1,0), nbrs(0,1,0), nbrs(0,0,-1), nbrs(0,0,1)};
    Real* const data[6] = {f.x0.data, f.x1.data, f.y0.data, f.y1.data, f.z0.data, f.z1.data};
    const int Nelem[6] = {f.x0._Nelem, f.x1._Nelem, f.y0._Nelem, f.y1._Nelem, f.z0._Nelem, f.z1._Nelem};
    const int tagbase = 0;
    for (int i = 0; i < 6; ++i)
    {
        const int dir  = i/2;
        const int side = (i+1)%2;
        if (src[i] >= 0)
        {
            MPI_Request req;
            MPI_Irecv(data[i], Nelem[i], MPIREAL, src[i], tagbase+dir*2+side, m_cartcomm, &req);
            f_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_send_edges(const MPInbr& nbrs, const Edges& e, std::vector<MPI_Request>& e_req) const
{
    const int dstX[4] = {nbrs(0,-1,-1), nbrs(0,1,-1), nbrs(0,1,1), nbrs(0,-1,1)};
    const int dstY[4] = {nbrs(-1,0,-1), nbrs(-1,0,1), nbrs(1,0,1), nbrs(1,0,-1)};
    const int dstZ[4] = {nbrs(-1,-1,0), nbrs(1,-1,0), nbrs(1,1,0), nbrs(-1,1,0)};
    Real* const dataX[4] = {e.x0.data, e.x1.data, e.x2.data, e.x3.data};
    Real* const dataY[4] = {e.y0.data, e.y1.data, e.y2.data, e.y3.data};
    Real* const dataZ[4] = {e.z0.data, e.z1.data, e.z2.data, e.z3.data};
    const int NelemX[4] = {e.x0._Nelem, e.x1._Nelem, e.x2._Nelem, e.x3._Nelem};
    const int NelemY[4] = {e.y0._Nelem, e.y1._Nelem, e.y2._Nelem, e.y3._Nelem};
    const int NelemZ[4] = {e.z0._Nelem, e.z1._Nelem, e.z2._Nelem, e.z3._Nelem};
    const int tagbase = 6;
    for (int i = 0; i < 4; ++i)
    {
        const int edge = i;
        if (dstX[i] >= 0)
        {
            MPI_Request req;
            MPI_Isend(dataX[i], NelemX[i], MPIREAL, dstX[i], tagbase+0*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
        if (dstY[i] >= 0)
        {
            MPI_Request req;
            MPI_Isend(dataY[i], NelemY[i], MPIREAL, dstY[i], tagbase+1*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
        if (dstZ[i] >= 0)
        {
            MPI_Request req;
            MPI_Isend(dataZ[i], NelemZ[i], MPIREAL, dstZ[i], tagbase+2*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_recv_edges(const MPInbr& nbrs, Edges& e, std::vector<MPI_Request>& e_req) const
{
    const int srcX[4] = {nbrs(0,-1,-1), nbrs(0,1,-1), nbrs(0,1,1), nbrs(0,-1,1)};
    const int srcY[4] = {nbrs(-1,0,-1), nbrs(-1,0,1), nbrs(1,0,1), nbrs(1,0,-1)};
    const int srcZ[4] = {nbrs(-1,-1,0), nbrs(1,-1,0), nbrs(1,1,0), nbrs(-1,1,0)};
    Real* const dataX[4] = {e.x0.data, e.x1.data, e.x2.data, e.x3.data};
    Real* const dataY[4] = {e.y0.data, e.y1.data, e.y2.data, e.y3.data};
    Real* const dataZ[4] = {e.z0.data, e.z1.data, e.z2.data, e.z3.data};
    const int NelemX[4] = {e.x0._Nelem, e.x1._Nelem, e.x2._Nelem, e.x3._Nelem};
    const int NelemY[4] = {e.y0._Nelem, e.y1._Nelem, e.y2._Nelem, e.y3._Nelem};
    const int NelemZ[4] = {e.z0._Nelem, e.z1._Nelem, e.z2._Nelem, e.z3._Nelem};
    const int tagbase = 6;
    for (int i = 0; i < 4; ++i)
    {
        const int edge = (i+2)%4;
        if (srcX[i] >= 0)
        {
            MPI_Request req;
            MPI_Irecv(dataX[i], NelemX[i], MPIREAL, srcX[i], tagbase+0*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
        if (srcY[i] >= 0)
        {
            MPI_Request req;
            MPI_Irecv(dataY[i], NelemY[i], MPIREAL, srcY[i], tagbase+1*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
        if (srcZ[i] >= 0)
        {
            MPI_Request req;
            MPI_Irecv(dataZ[i], NelemZ[i], MPIREAL, srcZ[i], tagbase+2*4+edge, m_cartcomm, &req);
            e_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_send_corners(const MPInbr& nbrs, const Corners& c, std::vector<MPI_Request>& c_req) const
{
    const int dst[8] = {nbrs(-1,-1,-1), nbrs(-1,1,-1), nbrs(-1,1,1), nbrs(-1,-1,1), nbrs(1,-1,-1), nbrs(1,1,-1), nbrs(1,1,1), nbrs(1,-1,1)};
    Real* const data[8] = {c.x00.data, c.x01.data, c.x02.data, c.x03.data, c.x10.data, c.x11.data, c.x12.data, c.x13.data};
    const int Nelem[8] = {c.x00._Nelem, c.x01._Nelem, c.x02._Nelem, c.x03._Nelem, c.x10._Nelem, c.x11._Nelem, c.x12._Nelem, c.x13._Nelem};
    const int tagbase = 18;
    for (int i = 0; i < 8; ++i)
    {
        const int cycl = i%4;
        const int side = i/4;
        if (dst[i] >= 0)
        {
            MPI_Request req;
            MPI_Isend(data[i], Nelem[i], MPIREAL, dst[i], tagbase+side*4+cycl, m_cartcomm, &req);
            c_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_recv_corners(const MPInbr& nbrs, Corners& c, std::vector<MPI_Request>& c_req) const
{
    const int src[8] = {nbrs(-1,-1,-1), nbrs(-1,1,-1), nbrs(-1,1,1), nbrs(-1,-1,1), nbrs(1,-1,-1), nbrs(1,1,-1), nbrs(1,1,1), nbrs(1,-1,1)};
    Real* const data[8] = {c.x00.data, c.x01.data, c.x02.data, c.x03.data, c.x10.data, c.x11.data, c.x12.data, c.x13.data};
    const int Nelem[8] = {c.x00._Nelem, c.x01._Nelem, c.x02._Nelem, c.x03._Nelem, c.x10._Nelem, c.x11._Nelem, c.x12._Nelem, c.x13._Nelem};
    const int tagbase = 18;
    for (int i = 0; i < 8; ++i)
    {
        const int cycl = (i+2)%4;
        const int side = (7-i)/4;
        if (src[i] >= 0)
        {
            MPI_Request req;
            MPI_Irecv(data[i], Nelem[i], MPIREAL, src[i], tagbase+side*4+cycl, m_cartcomm, &req);
            c_req.push_back(req);
        }
    }
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_faces(Faces& f, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(-1,0,0) >= 0) _pack_faceX(f.x0, 0);
    if (nbr(1,0,0) >= 0)  _pack_faceX(f.x1, Nx-Face_t::_P);

    if (nbr(0,-1,0) >= 0) _pack_faceY(f.y0, 0);
    if (nbr(0,1,0) >= 0)  _pack_faceY(f.y1, Ny-Face_t::_P);

    if (nbr(0,0,-1) >= 0) _pack_faceZ(f.z0, 0);
    if (nbr(0,0,1) >= 0)  _pack_faceZ(f.z1, Nz-Face_t::_P);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_unpack_faces(const Faces& f, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(-1,0,0) >= 0) _upack_faceX(f.x0, -Face_t::_P);
    else                  _halo_faceX(0, -Face_t::_P);
    if (nbr(1,0,0) >= 0)  _upack_faceX(f.x1, Nx);
    else                  _halo_faceX(Nx-1, Nx);

    if (nbr(0,-1,0) >= 0) _upack_faceY(f.y0, -Face_t::_P);
    else                  _halo_faceY(0, -Face_t::_P);
    if (nbr(0,1,0) >= 0)  _upack_faceY(f.y1, Ny);
    else                  _halo_faceY(Ny-1, Ny);

    if (nbr(0,0,-1) >= 0) _upack_faceZ(f.z0, -Face_t::_P);
    else                  _halo_faceZ(0, -Face_t::_P);
    if (nbr(0,0,1) >= 0)  _upack_faceZ(f.z1, Nz);
    else                  _halo_faceZ(Nz-1, Nz);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_edges(Edges& e, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(0,-1,-1) >= 0) _pack_edgeX(e.x0, 0, 0);
    if (nbr(0,1,-1) >= 0)  _pack_edgeX(e.x1, Ny-Edge_t::_P, 0);
    if (nbr(0,1,1) >= 0)   _pack_edgeX(e.x2, Ny-Edge_t::_P, Nz-Edge_t::_Q);
    if (nbr(0,-1,1) >= 0)  _pack_edgeX(e.x3, 0, Nz-Edge_t::_Q);

    if (nbr(-1,0,-1) >= 0) _pack_edgeY(e.y0, 0, 0);
    if (nbr(-1,0,1) >= 0)  _pack_edgeY(e.y1, 0, Nz-Edge_t::_Q);
    if (nbr(1,0,1) >= 0)   _pack_edgeY(e.y2, Nx-Edge_t::_P, Nz-Edge_t::_Q);
    if (nbr(1,0,-1) >= 0)  _pack_edgeY(e.y3, Nx-Edge_t::_P, 0);

    if (nbr(-1,-1,0) >= 0) _pack_edgeZ(e.z0, 0, 0);
    if (nbr(1,-1,0) >= 0)  _pack_edgeZ(e.z1, Nx-Edge_t::_P, 0);
    if (nbr(1,1,0) >= 0)   _pack_edgeZ(e.z2, Nx-Edge_t::_P, Ny-Edge_t::_Q);
    if (nbr(-1,1,0) >= 0)  _pack_edgeZ(e.z3, 0, Ny-Edge_t::_Q);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_unpack_edges(const Edges& e, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(0,-1,-1) >= 0) _upack_edgeX(e.x0, -Edge_t::_P, -Edge_t::_Q);
    else                   _halo_edgeX(0, 0, -Edge_t::_P, -Edge_t::_Q);
    if (nbr(0,1,-1) >= 0)  _upack_edgeX(e.x1, Ny, -Edge_t::_Q);
    else                   _halo_edgeX(Ny-1, 0, Ny, -Edge_t::_Q);
    if (nbr(0,1,1) >= 0)   _upack_edgeX(e.x2, Ny, Nz);
    else                   _halo_edgeX(Ny-1, Nz-1, Ny, Nz);
    if (nbr(0,-1,1) >= 0)  _upack_edgeX(e.x3, -Edge_t::_P, Nz);
    else                   _halo_edgeX(0, Nz-1, -Edge_t::_P, Nz);

    if (nbr(-1,0,-1) >= 0) _upack_edgeY(e.y0, -Edge_t::_P, -Edge_t::_Q);
    else                   _halo_edgeY(0, 0, -Edge_t::_P, -Edge_t::_Q);
    if (nbr(-1,0,1) >= 0)  _upack_edgeY(e.y1, -Edge_t::_P, Nz);
    else                   _halo_edgeY(0, Nz-1, -Edge_t::_P, Nz);
    if (nbr(1,0,1) >= 0)   _upack_edgeY(e.y2, Nx, Nz);
    else                   _halo_edgeY(Nx-1, Nz-1, Nx, Nz);
    if (nbr(1,0,-1) >= 0)  _upack_edgeY(e.y3, Nx, -Edge_t::_Q);
    else                   _halo_edgeY(Nx-1, 0, Nx, -Edge_t::_Q);

    if (nbr(-1,-1,0) >= 0) _upack_edgeZ(e.z0, -Edge_t::_P, -Edge_t::_Q);
    else                   _halo_edgeZ(0, 0, -Edge_t::_P, -Edge_t::_Q);
    if (nbr(1,-1,0) >= 0)  _upack_edgeZ(e.z1, Nx, -Edge_t::_Q);
    else                   _halo_edgeZ(Nx-1, 0, Nx, -Edge_t::_Q);
    if (nbr(1,1,0) >= 0)   _upack_edgeZ(e.z2, Nx, Ny);
    else                   _halo_edgeZ(Nx-1, Ny-1, Nx, Ny);
    if (nbr(-1,1,0) >= 0)  _upack_edgeZ(e.z3, -Edge_t::_P, Ny);
    else                   _halo_edgeZ(0, Ny-1, -Edge_t::_P, Ny);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_corners(Corners& c, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(-1,-1,-1) >= 0) _pack_corner(c.x00, 0, 0, 0);
    if (nbr(-1,1,-1) >= 0)  _pack_corner(c.x01, 0, Ny-Corner_t::_Q, 0);
    if (nbr(-1,1,1) >= 0)   _pack_corner(c.x02, 0, Ny-Corner_t::_Q, Nz-Corner_t::_R);
    if (nbr(-1,-1,1) >= 0)  _pack_corner(c.x03, 0, 0, Nz-Corner_t::_R);

    if (nbr(1,-1,-1) >= 0) _pack_corner(c.x10, Nx-Corner_t::_P, 0, 0);
    if (nbr(1,1,-1) >= 0)  _pack_corner(c.x11, Nx-Corner_t::_P, Ny-Corner_t::_Q, 0);
    if (nbr(1,1,1) >= 0)   _pack_corner(c.x12, Nx-Corner_t::_P, Ny-Corner_t::_Q, Nz-Corner_t::_R);
    if (nbr(1,-1,1) >= 0)  _pack_corner(c.x13, Nx-Corner_t::_P, 0, Nz-Corner_t::_R);
}


template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_unpack_corners(const Corners& c, const MPInbr& nbr)
{
    const int Nx = this->m_data.Nx();
    const int Ny = this->m_data.Ny();
    const int Nz = this->m_data.Nz();

    if (nbr(-1,-1,-1) >= 0) _upack_corner(c.x00, -Corner_t::_P, -Corner_t::_Q, -Corner_t::_R);
    else                    _halo_corner(0, 0, 0, -Corner_t::_P, -Corner_t::_Q, -Corner_t::_R);
    if (nbr(-1,1,-1) >= 0)  _upack_corner(c.x01, -Corner_t::_P, Ny, -Corner_t::_R);
    else                    _halo_corner(0, Ny-1, 0, -Corner_t::_P, Ny, -Corner_t::_R);
    if (nbr(-1,1,1) >= 0)   _upack_corner(c.x02, -Corner_t::_P, Ny, Nz);
    else                    _halo_corner(0, Ny-1, Nz-1, -Corner_t::_P, Ny, Nz);
    if (nbr(-1,-1,1) >= 0)  _upack_corner(c.x03, -Corner_t::_P, -Corner_t::_Q, Nz);
    else                    _halo_corner(0, 0, Nz-1, -Corner_t::_P, -Corner_t::_Q, Nz);

    if (nbr(1,-1,-1) >= 0) _upack_corner(c.x10, Nx, -Corner_t::_Q, -Corner_t::_R);
    else                   _halo_corner(Nx-1, 0, 0, Nx, -Corner_t::_Q, -Corner_t::_R);
    if (nbr(1,1,-1) >= 0)  _upack_corner(c.x11, Nx, Ny, -Corner_t::_R);
    else                   _halo_corner(Nx-1, Ny-1, 0, Nx, Ny, -Corner_t::_R);
    if (nbr(1,1,1) >= 0)   _upack_corner(c.x12, Nx, Ny, Nz);
    else                   _halo_corner(Nx-1, Ny-1, Nz-1, Nx, Ny, Nz);
    if (nbr(1,-1,1) >= 0)  _upack_corner(c.x13, Nx, -Corner_t::_Q, Nz);
    else                   _halo_corner(Nx-1, 0, Nz-1, Nx, -Corner_t::_Q, Nz);
}

// PUP kernels
#include "InterpolatorPUPMPI.h"

#endif /* INTERPOLATORMPI_H_FKS9KH2J */
