// File       : easy_iso_MPI.cpp
// Date       : Mon Nov 28 16:55:33 2016
// Author     : Fabian Wermelinger
// Description: easy iso extraction with MPI
// Copyright 2016 ETH Zurich. All Rights Reserved.
#include <mpi.h>
#include <string>
#include <sstream>
#include "ArgumentParser.h"
#include "interpolation/kernels.h"
#include "IsoExtractorMPI.h"

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    ArgumentParser parser(argc, const_cast<const char**>(argv));

    using Tinterp = M2;
    if (parser("interp_kernel").asString("M2") == "Mp4")
        using Tinterp = Mprime4;
    else if (parser("interp_kernel").asString("M2") == "M5")
        using Tinterp = M5;

    IsoExtractorMPI<Tinterp> iso(parser);

    const Real isoval = parser("isoval").asDouble(0.0);
    std::ostringstream basename;
    basename << "isosurf_" << isoval;
    const std::string fout = parser("fout").asString(basename.str());
    iso.extract(isoval, fout);

    MPI_Finalize();

    return 0;
}
