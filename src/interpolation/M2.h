// File       : M2.h
// Date       : Thu Nov 24 16:14:51 2016
// Author     : Fabian Wermelinger
// Description: M2 interpolation kernel (Monaghan 19885) -- triangular function
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef M2_H_LQ69GRM8
#define M2_H_LQ69GRM8

#include <cmath>
#include "common.h"

class M2
{
public:
    static constexpr int start = 0;
    static constexpr int end   = 2;

    inline Real operator()(const Real x) const
    {
        const Real IxI = std::abs(x);
        if (IxI <= 1.0)
            return 1.0 - IxI;
        else
            return 0.0;
    }
};

#endif /* M2_H_LQ69GRM8 */
