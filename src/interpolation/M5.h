// File       : M5.h
// Date       : Thu Nov 24 09:33:24 2016
// Author     : Fabian Wermelinger
// Description: M5 smoothing interpolation kernel (Monaghan 1985)
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef M5_H_G6FRPHTC
#define M5_H_G6FRPHTC

#include <cmath>
#include "common.h"

class M5
{
public:
    static constexpr int start = -2;
    static constexpr int end   = 4;

    inline Real operator()(const Real x) const
    {
        const Real a0 = 1.0/48.0;
        const Real IxI = std::abs(x);
        if (IxI < 0.5)
            return a0*(43.125-75.0*IxI*IxI+42.0*IxI*IxI*IxI*IxI);
        else if (IxI < 1.5)
            return a0*(41.25+20.0*IxI-150.0*IxI*IxI+120.0*IxI*IxI*IxI-28.0*IxI*IxI*IxI*IxI);
        else if (IxI < 2.5)
            return a0*(IxI-2.5)*(IxI-2.5)*(IxI-2.5)*(7.0*IxI-7.5);
        else
            return 0.0;
    }
};

#endif /* M5_H_G6FRPHTC */
