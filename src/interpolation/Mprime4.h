// File       : Mprime4.h
// Date       : Thu Nov 24 08:33:08 2016
// Author     : Fabian Wermelinger
// Description: M^\prime_4 interpolation kernel (3rd order)
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef MPRIME4_H_KXJZRENA
#define MPRIME4_H_KXJZRENA

#include <cmath>
#include "common.h"

class Mprime4
{
public:
    static constexpr int start = -1;
    static constexpr int end   = 3;

    inline Real operator()(const Real x) const
    {
        const Real IxI = std::abs(x);
        if (IxI < 1.0)
            return 0.5*(IxI - 1.0)*(3.0*IxI*IxI - 2.0*IxI - 2.0);
        else if (IxI < 2.0)
            return -0.5*(IxI - 1.0)*(IxI - 2.0)*(IxI - 2.0);
        else
            return 0.0;
    }
};

#endif /* MPRIME4_H_KXJZRENA */
