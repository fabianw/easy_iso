// File       : kernels.h
// Date       : Thu Nov 24 16:47:33 2016
// Author     : Fabian Wermelinger
// Description: Interpolation kernels
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef KERNELS_H_IRWGGDVC
#define KERNELS_H_IRWGGDVC

#include "interpolation/M2.h"
#include "interpolation/Mprime4.h"
#include "interpolation/M5.h"

#endif /* KERNELS_H_IRWGGDVC */
