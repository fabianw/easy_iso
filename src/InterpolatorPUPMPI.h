// File       : InterpolatorPUPMPI.h
// Date       : Tue Nov 29 21:00:01 2016
// Author     : Fabian Wermelinger
// Description: pack/unpack kernels
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef INTERPOLATORPUPMPI_H_EPMRY0VT
#define INTERPOLATORPUPMPI_H_EPMRY0VT

///////////////////////////////////////////////////////////////////////////////
// FACES
///////////////////////////////////////////////////////////////////////////////
template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_faceX(func_t& eval)
{
    for (int iz=0; iz<this->m_data.Nz(); ++iz)
        for (int iy=0; iy<this->m_data.Ny(); ++iy)
            for (int ix=0; ix<Face_t::_P; ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_faceY(func_t& eval)
{
    for (int iz=0; iz<this->m_data.Nz(); ++iz)
        for (int iy=0; iy<Face_t::_P; ++iy)
            for (int ix=0; ix<this->m_data.Nx(); ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_faceZ(func_t& eval)
{
    for (int iz=0; iz<Face_t::_P; ++iz)
        for (int iy=0; iy<this->m_data.Ny(); ++iy)
            for (int ix=0; ix<this->m_data.Nx(); ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_faceX(Face_t& f, const int _s0)
{
    func_t pack = [this, &f, _s0](const int ix, const int iy, const int iz) { f(iy,iz,ix) = this->m_data(ix+_s0,iy,iz); };
    _process_faceX(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_faceY(Face_t& f, const int _s0)
{
    func_t pack = [this, &f, _s0](const int ix, const int iy, const int iz) { f(ix,iz,iy) = this->m_data(ix,iy+_s0,iz); };
    _process_faceY(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_faceZ(Face_t& f, const int _s0)
{
    func_t pack = [this, &f, _s0](const int ix, const int iy, const int iz) { f(ix,iy,iz) = this->m_data(ix,iy,iz+_s0); };
    _process_faceZ(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_faceX(const Face_t& f, const int _d0)
{
    func_t unpack = [this, &f, _d0](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy,iz) = f(iy,iz,ix); };
    _process_faceX(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_faceY(const Face_t& f, const int _d0)
{
    func_t unpack = [this, &f, _d0](const int ix, const int iy, const int iz) { this->m_data(ix,iy+_d0,iz) = f(ix,iz,iy); };
    _process_faceY(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_faceZ(const Face_t& f, const int _d0)
{
    func_t unpack = [this, &f, _d0](const int ix, const int iy, const int iz) { this->m_data(ix,iy,iz+_d0) = f(ix,iy,iz); };
    _process_faceZ(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_faceX(const int _s0, const int _d0)
{
    func_t halo = [this, _s0, _d0](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy,iz) = this->m_data(_s0,iy,iz); };
    _process_faceX(halo);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_faceY(const int _s0, const int _d0)
{
    func_t halo = [this, _s0, _d0](const int ix, const int iy, const int iz) { this->m_data(ix,iy+_d0,iz) = this->m_data(ix,_s0,iz); };
    _process_faceY(halo);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_faceZ(const int _s0, const int _d0)
{
    func_t halo = [this, _s0, _d0](const int ix, const int iy, const int iz) { this->m_data(ix,iy,iz+_d0) = this->m_data(ix,iy,_s0); };
    _process_faceZ(halo);
}

///////////////////////////////////////////////////////////////////////////////
// EDGES
///////////////////////////////////////////////////////////////////////////////
template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_edgeX(func_t& eval)
{
    for (int iz=0; iz<Edge_t::_Q; ++iz)
        for (int iy=0; iy<Edge_t::_P; ++iy)
            for (int ix=0; ix<this->m_data.Nx(); ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_edgeY(func_t& eval)
{
    for (int iz=0; iz<Edge_t::_Q; ++iz)
        for (int iy=0; iy<this->m_data.Ny(); ++iy)
            for (int ix=0; ix<Edge_t::_P; ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_edgeZ(func_t& eval)
{
    for (int iz=0; iz<this->m_data.Nz(); ++iz)
        for (int iy=0; iy<Edge_t::_Q; ++iy)
            for (int ix=0; ix<Edge_t::_P; ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_edgeX(Edge_t& e, const int _s0, const int _s1)
{
    func_t pack = [this, &e, _s0, _s1](const int ix, const int iy, const int iz) { e(ix,iy,iz) = this->m_data(ix,iy+_s0,iz+_s1); };
    _process_edgeX(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_edgeY(Edge_t& e, const int _s0, const int _s1)
{
    func_t pack = [this, &e, _s0, _s1](const int ix, const int iy, const int iz) { e(iy,ix,iz) = this->m_data(ix+_s0,iy,iz+_s1); };
    _process_edgeY(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_edgeZ(Edge_t& e, const int _s0, const int _s1)
{
    func_t pack = [this, &e, _s0, _s1](const int ix, const int iy, const int iz) { e(iz,ix,iy) = this->m_data(ix+_s0,iy+_s1,iz); };
    _process_edgeZ(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_edgeX(const Edge_t& e, const int _d0, const int _d1)
{
    func_t unpack = [this, &e, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix,iy+_d0,iz+_d1) = e(ix,iy,iz); };
    _process_edgeX(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_edgeY(const Edge_t& e, const int _d0, const int _d1)
{
    func_t unpack = [this, &e, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy,iz+_d1) = e(iy,ix,iz); };
    _process_edgeY(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_edgeZ(const Edge_t& e, const int _d0, const int _d1)
{
    func_t unpack = [this, &e, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy+_d1,iz) = e(iz,ix,iy); };
    _process_edgeZ(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_edgeX(const int _s0, const int _s1, const int _d0, const int _d1)
{
    func_t halo = [this, _s0, _s1, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix,iy+_d0,iz+_d1) = this->m_data(ix,_s0,_s1); };
    _process_edgeX(halo);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_edgeY(const int _s0, const int _s1, const int _d0, const int _d1)
{
    func_t halo = [this, _s0, _s1, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy,iz+_d1) = this->m_data(_s0,iy,_s1); };
    _process_edgeY(halo);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_edgeZ(const int _s0, const int _s1, const int _d0, const int _d1)
{
    func_t halo = [this, _s0, _s1, _d0, _d1](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy+_d1,iz) = this->m_data(_s0,_s1,iz); };
    _process_edgeZ(halo);
}

///////////////////////////////////////////////////////////////////////////////
// CORNERS
///////////////////////////////////////////////////////////////////////////////
template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_process_corner(func_t& eval)
{
    for (int iz=0; iz<Corner_t::_R; ++iz)
        for (int iy=0; iy<Corner_t::_Q; ++iy)
            for (int ix=0; ix<Corner_t::_P; ++ix)
                eval(ix,iy,iz);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_pack_corner(Corner_t& c, const int _s0, const int _s1, const int _s2)
{
    func_t pack = [this, &c, _s0, _s1, _s2](const int ix, const int iy, const int iz) { c(ix,iy,iz) = this->m_data(ix+_s0,iy+_s1,iz+_s2); };
    _process_corner(pack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_upack_corner(const Corner_t& c, const int _d0, const int _d1, const int _d2)
{
    func_t unpack = [this, &c, _d0, _d1, _d2](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy+_d1,iz+_d2) = c(ix,iy,iz); };
    _process_corner(unpack);
}

template <typename Tinterp>
void InterpolatorMPI<Tinterp>::_halo_corner(const int _s0, const int _s1, const int _s2, const int _d0, const int _d1, const int _d2)
{
    func_t halo = [this, _s0, _s1, _s2, _d0, _d1, _d2](const int ix, const int iy, const int iz) { this->m_data(ix+_d0,iy+_d1,iz+_d2) = this->m_data(_s0,_s1,_s2); };
    _process_corner(halo);
}

#endif /* INTERPOLATORPUPMPI_H_EPMRY0VT */
