// File       : common.h
// Date       : Tue Nov 22 15:51:55 2016
// Author     : Fabian Wermelinger
// Description: common stuff
// Copyright 2016 ETH Zurich. All Rights Reserved.
#ifndef COMMON_H_ZH0IVQBA
#define COMMON_H_ZH0IVQBA

#include <cassert>

#ifdef _FLOAT_PRECISION_
typedef float Real;
#else
typedef double Real;
#endif

#ifdef _USE_HDF_
#ifdef _FLOAT_PRECISION_
#define HDF_PRECISION H5T_NATIVE_FLOAT
#else
#define HDF_PRECISION H5T_NATIVE_DOUBLE
#endif
#endif /* _USE_HDF_ */

#ifndef _ALIGN_
#define _ALIGN_ 16
#endif /* _ALIGN_ */

#include "Matrix3D.h"


template <typename T, size_t P>
struct Face
{
    T* data;
    int _N;
    int _M;
    int _Nelem;
    static constexpr int _P = P;
    Face() : data(nullptr) {}
    Face(const size_t N, const size_t M) :
        data(new T[N*M*P]),
        _N(N), _M(M), _Nelem(N*M*P)
    {}
    ~Face() { delete [] data; }

    void alloc(const size_t N, const size_t M)
    {
        if (data == nullptr)
        {
            _N = N;
            _M = M;
            _Nelem = N*M*P;
            data = new T[_Nelem];
        }
    }

    inline T& operator()(const size_t ix, const size_t iy, const size_t iz)
    {
        assert(ix < _N);
        assert(iy < _M);
        assert(iz < P);
        return data[ix + iy*_N + iz*_N*_M];
    }

    inline T operator()(const size_t ix, const size_t iy, const size_t iz) const
    {
        assert(ix < _N);
        assert(iy < _M);
        assert(iz < P);
        return data[ix + iy*_N + iz*_N*_M];
    }
};


template <typename T, size_t P, size_t Q>
struct Edge
{
    T* data;
    int _N;
    int _Nelem;
    static constexpr int _P = P;
    static constexpr int _Q = Q;
    Edge() : data(nullptr) {}
    Edge(const size_t N) :
        data(new T[N*P*Q]),
        _N(N), _Nelem(N*P*Q)
    {}
    ~Edge() { delete [] data; }

    void alloc(const size_t N)
    {
        if (data == nullptr)
        {
            _N = N;
            _Nelem = N*P*Q;
            data = new T[_Nelem];
        }
    }

    inline T& operator()(const size_t ix, const size_t iy, const size_t iz)
    {
        assert(ix < _N);
        assert(iy < P);
        assert(iz < Q);
        return data[ix + iy*_N + iz*_N*P];
    }

    inline T operator()(const size_t ix, const size_t iy, const size_t iz) const
    {
        assert(ix < _N);
        assert(iy < P);
        assert(iz < Q);
        return data[ix + iy*_N + iz*_N*P];
    }
};


template <typename T, size_t P, size_t Q, size_t R>
struct Corner
{
    T* data;
    const int _Nelem;
    static constexpr int _P = P;
    static constexpr int _Q = Q;
    static constexpr int _R = R;
    Corner() : data(new T[P*Q*R]), _Nelem(P*Q*R) {}
    ~Corner() { delete [] data; }

    inline T& operator()(const size_t ix, const size_t iy, const size_t iz)
    {
        assert(ix < P);
        assert(iy < Q);
        assert(iz < R);
        return data[ix + iy*P + iz*P*Q];
    }

    inline T operator()(const size_t ix, const size_t iy, const size_t iz) const
    {
        assert(ix < P);
        assert(iy < Q);
        assert(iz < R);
        return data[ix + iy*P + iz*P*Q];
    }
};


typedef Matrix3D<Real,-3,3,-3,3,-3,3> Matrix_t;
typedef Face<Real,3> Face_t;
typedef Edge<Real,3,3> Edge_t;
typedef Corner<Real,3,3,3> Corner_t;


struct Faces
{
    // indices 0 and 1 correspond to the near and far faces relative to the
    // origin.
    Face_t x0, x1, y0, y1, z0, z1;
    Faces() {}
    Faces(const size_t XN, const size_t XM,
            const size_t YN, const size_t YM,
            const size_t ZN, const size_t ZM) :
        x0(XN,XM), x1(XN,XM), y0(YN,YM), y1(YN,YM), z0(ZN,ZM), z1(ZN,ZM)
    {}
    void alloc(const size_t XN, const size_t XM,
            const size_t YN, const size_t YM,
            const size_t ZN, const size_t ZM)
    {
        x0.alloc(XN,XM);
        x1.alloc(XN,XM);
        y0.alloc(YN,YM);
        y1.alloc(YN,YM);
        z0.alloc(ZN,ZM);
        z1.alloc(ZN,ZM);
    }
};

struct Edges
{
    // edges are along the given coordinate direction. The counting is done
    // using right hand rule for the indicated direction.  The 0-edge is the
    // one that goes through the origin.
    Edge_t x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, z3;
    Edges() {}
    Edges(const size_t XN, const size_t YN, const size_t ZN) :
        x0(XN), x1(XN), x2(XN), x3(XN),
        y0(YN), y1(YN), y2(YN), y3(YN),
        z0(ZN), z1(ZN), z2(ZN), z3(ZN)
    {}
    void alloc(const size_t XN, const size_t YN, const size_t ZN)
    {
        x0.alloc(XN); x1.alloc(XN); x2.alloc(XN); x3.alloc(XN);
        y0.alloc(YN); y1.alloc(YN); y2.alloc(YN); y3.alloc(YN);
        z0.alloc(ZN); z1.alloc(ZN); z2.alloc(ZN); z3.alloc(ZN);
    }

};

struct Corners
{
    // corners are identified by x0 or x1 (near far).  The second index is
    // again given by the right hand rule along the x-axis.  The x{0,1}0 are
    // the ones along the axis that goes through the origin.
    Corner_t x00, x01, x02, x03, x10, x11, x12, x13;
};

#endif /* COMMON_H_ZH0IVQBA */
